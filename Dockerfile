FROM registry.gitlab.com/ulrichschreiner/base/ubuntu:21.04

RUN apt update && \
    apt -y install ca-certificates && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /etc/vector

COPY vector/target/release/vector /usr/bin/vector
VOLUME /var/lib/vector/

RUN ["vector", "--version"]

ENTRYPOINT ["/usr/bin/vector"]
